;;; init-pdf.el --- PDF viewer configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


;;; PDF tools
(maybe-require-package 'pdf-tools)

;; use it for latex documents after compilation
(add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
(setq TeX-view-program-selection '((output-pdf "PDF Tools")))
(setq TeX-source-correlate-start-server t)
;;(add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode)
;; no line numbers when in PDF view mode
(add-hook 'pdf-view-mode-hook #'(lambda ()
                                  (display-line-numbers-mode -1)))


;; On demand loading, leads to faster startup time
(pdf-loader-install)


;;; function to select pages and "print" them as a separated pdf
(defvar selected-pages '())
(defun select-page ()
  "Add current page to list of selected page;s."
  (interactive)
  (add-to-list 'selected-pages (pdf-view-current-page) t))
(defun extract-selected-pages (file)
  "Save selected pages to FILE."
  (interactive "FSave as: ")
  (setq selected-pages (sort selected-pages #'<))
  (start-process "pdfjam" "*pdfjam*"
                 "pdfjam"
                 (buffer-file-name)
                 (mapconcat #'number-to-string
                            selected-pages
                            ",")
                 "-o"
                 (expand-file-name file)))

(provide 'init-pdf)
;;; init-pdf.el ends here
