;;; init-latex.el --- LaTeX configuration,
;;; including org-mode export -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


;;; Auctex mode
(maybe-require-package 'auctex)
(maybe-require-package 'ox-latex)
(when (locate-library "auctex")
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq TeX-source-correlate-start-server t)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (setq reftex-plug-into-AUCTeX t)
  )

(setq reftex-use-external-file-finders t)
(setq reftex-external-file-finders
      '(("tex" . "kpsewhich -format=.tex %f")
        ("bib" . "kpsewhich -format=.bib %f")))

(add-hook 'LaTeX-mode-hook
          (lambda ()
            (LaTeX-add-environments
             '("theorem" LaTeX-env-label)
             '("proposition" LaTeX-env-label)
             '("lemma" LaTeX-env-label)
             '("definition" LaTeX-env-label)
             '("example" LaTeX-env-label)
             '("remark" LaTeX-env-label))))


;;; Company LaTeX
(when (maybe-require-package 'LaTeX-mode)
  (when (maybe-require-package 'company-auctex)
    (after-load 'company
      (after-load 'LaTeX
        (push 'company-auctex company-backends))))
  (when (maybe-require-package 'company-bibtex)
    (after-load 'company
      (after-load 'LaTeX
        (push 'company-bibtex company-backends))))
  (when (maybe-require-package 'company-reftex)
    (after-load 'company
      (after-load 'LaTeX
        (push 'company-reftex company-backends))))
  (when (maybe-require-package 'company-math)
    (after-load 'company
      (after-load 'LaTeX
        (push 'company-math company-backends)))))


;;; Spell ckecer configuration
(setq ispell-program-name "aspell")
(global-set-key (kbd "C-c o") 'ispell)
(setq ispell-dictionary "english")
;; define a function to switch between dictionaries
;; for the french dictionary, a aspell-fr package
;; must be installed on the host system
(defun fd-switch-dictionary()
  "Keybinding for switching between dictionary languages."
  (interactive)
  (let* ((dic ispell-current-dictionary)
         (change (if (string= dic "french") "english" "french")))
    (ispell-change-dictionary change)
    (message "Dictionary switched from %s to %s" dic change)))
(global-set-key (kbd "<f8>")   'fd-switch-dictionary)



;;; Minted code block settings
(setq org-latex-src-block-backend 'minted)
(add-to-list 'org-latex-packages-alist '("newfloat" "minted"))
(setq org-latex-minted-options
      '(("frame" "single")
        ("framesep" "2mm")
        ("baselinestretch" "1.2")
        ("linenos")
        ("breaklines")
        ("fontsize" "\\footnotesize")))

;; shell escape setting for minted in regular latex file
(setq LaTeX-command-style '(("" "%(PDF)%(latex) -shell-escape %S%(PDFout)")))

;; options for org exportation to LaTeX
(setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))


(provide 'init-latex)
;;; init-latex.el ends here
