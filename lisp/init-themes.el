;;; init-themes.el --- Defaults for themes -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(setq default-frame-alist '((font . "DejaVu Sans Mono-15"))) ;; uses a standard and pretty font
(set-fontset-font "fontset-default" 'han  "Kochi Gothic")
(set-fontset-font "fontset-default" 'kana "Kochi Gothic")

(maybe-require-package 'nord-theme)
(load-theme 'nord t)

(provide 'init-themes)
;;; init-themes.el ends here
